/*Writing comments in Javascript*/
// one-line comments (Ctrl + /)
/*
	multi-line comments (Ctrl + Shift + /)
*/
console.log("Hello Batch 144!!");
/*
	Javascript - we can see or log messages in our console.

	Browser onsoles are part of our consoles which will allow us to see/log messages, data, or information from our programming language - Javascript. 

	For most browsers, consoles are easily accessed through its developer tools in the console tab.

	In fact, consoles in browsers will allow us to add some Javascript expressions.

	Statements

	Statements are instructions, expressions that we add to our programmingh language to communicate with our computers.

	JS statements usually ends in a semicolon (;). However, JS has implemented a way to automatically add semicolons at the end of a line/statement. Semicolonss can actually be omitted in creating JS statements but semicolons in JS are added to mark the end of the statement.

	Syntax 

	Syntax in programming is a set of rules that describes how statements are properly made/constructed.

	Lines / blocks of code must folow a certain of rules for it to work properly.

*/

console.log("Roy Christian Pery");

/*console.log("Sisig");
console.log("Sisig");
console.log("Sisig");*/

let food1 = "Sisig";

console.log(food1);

/*
	Variables are a way to store information or data within our JS. 

	To create a variable, we first declare the name of the variable with either the let/const keyword:

	let <nameOfVariable>;

	Then, we can initialize the variable with a value or data.

	let nameOfVariable = <data>;

*/

console.log("My favorite food is " + food1);

let food2 = "Dinuguan";

console.log("My favorite foods are " + food1 + " and " + food2);

let food3;

/*
	We can craete variables without adding an initial value, howver, the variable's content is undefined.

*/


console.log(food3);

food3 = "Chickenjoy";

/*
	We can updated the content of a let variable by reassigning the content using an assignment operator (=).

	Assignment operator (=) = lets us assign data to a variable.

*/

console.log(food3);

food1 = "Chicken Inasal";
food2 = "Lechon";

console.log(food1);
console.log(food2);

/*
	We can update our variables with an assignment operator without needing to use a "let" keyword.

	We cannot create another variable with the same name. It will result in an error.

*/

// let food1 = "Flat Tops"; // error

// const keyword

/*
	const keyword will also allow us to create variables. However, with the const keyword, we create constant variables which means these data to be saved in a constant variable will not be changed, channot be changed and should not be changed.
*/

const pi = 3.1416;

console.log(pi);

// Trying to re-assign a const variable will result into an error.

/*pi = "Pizza";

console.log(pi);

*/

/*const gravity;

console.log(gravity);*/

// We also cannot declare/create a const variable without initialization or an initial value.

let myName;
const sunriseDirection = "East";
const sunsetDirection = "West";
/*You can actually log multiple items, variables, data in console.log separated by ",". */
console.log(myName, sunriseDirection, sunsetDirection);

/*
	Guides in creating a JS variable:

	1. We can create a let variable with the "let" keyword. let variables can be reassigned but not redeclared.

	2. Creating a variable has two parts: Declaration of the variable name and initialization of the initial value of the variable using an assignment operator (=).

	3. A let variable can be declared without initialization. However, the value of the variable will be undefined until it is re-assigned with a value.

	4. Not Defined vs Undefined. Not Defined error means the variable is used but NOT declared. undefined results from a variable that is used but is not initialized.

	5. We can use const keyword to create contant variables. Constant variables cannot be declared without initialization. Constant variables cannot be re-assigned.

	6. When creating variable names, start with small caps, this is because of avoiding conflict with syntax in JS that is named with capital letters.

	7. If the variable would need two words, the naming convention is the use of camelCase. DO NOT ADD A SPACE for variables with words as names.

*/

/*Data types*/

/*
	strings are data which are alphanumerical text. It ciould be a name, a phrase, or even a sentence. We can create strings with single quote ('') or with double quote ("").

	variable = "string data";

*/

console.log("Sample String Data");

let country = "Philippines";
let province = "Rizal";

console.log(province, country);

/*
	Concatenation - is a way for us to add strings together and have a single string. We can use our "+" symbol for this.

*/

let fullAddress = province + "," + country;

console.log(fullAddress);

let greeting = "I live in " + country;

console.log(greeting);

/*In JS, when you use the + sign with strings, we have concatenation.*/

let numString = "50";
let numString2 = "25";

console.log(numString + numString2);

/*
	Strings have a property called .length and it tells us the number of characters in the string.

	Spaces, commas, periods can also be characters when included in a string.

	It will return a number type data.
*/

let hero = "Captain America";

console.log(hero.length);

/*Number Type*/
/*
	Number type data can actually be used in proper mathematical equations and operations.

*/
let students = 16;
console.log(students);

let num1 = 50;
let num2 = 25;

/*

	Addition operator will allow us to add two number types and return the sum as a number data type.
	
	Operations return a value and that value can be saved in a variable.

	What if we use the addition operator on a number type and a numberical string? It results to concatenation.

	parseInt() will allow us to turn a numeric string into a proper number type.
*/

let sum1 = num1 + num2;

console.log(sum1);

let numString3 = "100";

let sum2 = parseInt(numString3) + num1;

console.log(sum2);

const name = "Jeff";
console.log(num1, numString3);

let sum3 = sum1 + sum2;
let sum4 = parseInt(numString2) + num2;

console.log(sum3, sum4);

/*
	Subtraction operator

	Will let us get a difference between two number types. It will also result to a proper number data type.

	When subtraction operator is used on a string and a number, the string will be converted to a number automatically and then JS will perform the operation.

	This automatic conversion from one type to another is called Type conversion or Type coercion or Forced coercion.

	When a text string is subtracted with a number, it will result in NaN or "Not a Number" because JS converted the text string, it results to NaN and NaN-number = NaN 

*/
let difference = num1 - num2
console.log(difference);

let difference2 = numString3 - num2;
console.log(difference2);

let difference3 = hero - num2;
console.log(difference3);

let string1 = "fifteen";
console.log(num2 - string1);

/*Multiplication operator (*)*/

let num3 = 10;
let num4 = 5;
let product = num3 * num4;
console.log(product);
let product2 = numString3 * num3
console.log(product2);
let product3 = numString * numString3;
console.log(product3);

/*Division operator (/)*/
let num5 = 30;
let num6 = 3;
let quotient = num5 / num6;
console.log(quotient);
let quotient2 = numString3 / 5
console.log(quotient2);
let quotient3 = 450/num4;
console.log(quotient3);

/*Boolean (true or false)*/
/*Boolean is usually used for logical operations or for if-else conditions.*/
/*Naming convention for a variable that contains boolean is a yes or no question.*/

let isAdmin = true;
let isMarried = false;

/*
	You can actually name your variables any way you want, however, as the best practice, it should be:

	1. appropriate
	2. definitive of the value it contains.
	3. semantically correct.

*/

/*Arrays*/
/*
	Arrays are a special kind of data type wherein we can store multiple values. An array can strore multiple values and even of different types.
	
	For best practice, keep the data type of items in an array uniform.

	Values in an array are separated by comma. Failing to do so will result in an error.

	An array is created with an Array Literal ([]).
*/

let koponanNiEugene = ["Eugene", "Alfred", "Dennis", "Vincent"];

console.log(koponanNiEugene);
/*
	Array indeces are markers of the order of the items in the array. Array index start at 0.

	To access an array item: arrayName[0]
*/

console.log(koponanNiEugene[0]);
// Bad practice for an Array.
let array2 = ["One Punch Man", true, 500, "Saitama"];
console.log(array2);

// Objects
/*
	Objects are another kind of special data type used to mimic real world objects.

	With this, we can add information that makes sense, thematically relevant, and of different data types.

	Objects can be created with Object Literals ({}).

	Each value is given a label which makes the data significant and meaningful. This pairing of data and label is what we call a Key-Value pair.

	A key-value pair together is called a property.

	Each property is separated by a comma.

*/

let person1 = {
	heroName: "One Punch Man",
	isRegistered: true,
	salary: 500,
	realName: "Saitama"
};
// To get the value of an object's property, we can access it using dot notation.
// objectName.propertyName
console.log(person1.realName);

let myFavoriteBands = ["My Chemical Romance", "Paramore", "Parokya Ni Edgar", "Kamikazee"];

let me = {
	firstName: "Roy Christian",
	lastName: "Pery",
	isWebDeveloper: true,
	hasPortfolio: true,
	age: 24
	/*Properties of an object should relate to each other and thematically relevant to describe a single item/object.*/
};

/*undefined vs null*/

/*
	Null is the explicit declaration that there is no value.
*/

let sampleNull = null;

/*
	Undefined simply means that the variable exists, however a value was not initialized with the variable.
*/

let undefinedSample;

console.log(undefinedSample);

/*
	Certain processes in programming explicitly returns null to indicate that the task resulted to nothing.
*/

let foundResult = null;

// For undefined, this is normally caused by developers creating variables that have no value associated with them.
// The variable does exist, but its value is still unknown.

let person2 = {
	name: "Patricia",
	age: 28
}

// Because the variable person2 does exist however the property isAdmin does not.
console.log(person2.isAdmin);