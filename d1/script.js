/*Functions*/

function printStar(){
	console.log("*");
	console.log("**");
	console.log("***");
}

function sayHello(name){
	console.log("Hello" + name);
}

sayHello(6);

/*function alertPrint(){
	alert("Hello");
	sayHello();
}*/

/*Function that accepts two numbers and prints the sum*/

/*Parameters*/
// function addSum(x, y){
// 	let sum = x + y;
// 	console.log(sum);
// }

// addSum(13, 2); // arguments
// addSum(23, 56);

/*Function with 3 parameters*/
/*String Template literals*/
/*Interpolation*/
function printBio(lname, fname, age){
	// console.log("Hello" + lname + " " + fname + " " + age);
	console.log(`Hello Mr. ${lname}, ${fname}. He is ${age}.`);
}

printBio("Stark", "Tony", 50);

// Return keyword
function addSum(x, y){
	return y - x;
	console.log(x + y);
}

let sumNum = addSum(3,4);