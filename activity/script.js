function getBio(lname, fname, email, mobileNum){
	return `Hello! I am ${fname} ${lname}.\nYou can contact me via ${email}.\nMobile number: ${mobileNum}.`;
}

let output = getBio("Rogers", "Steve", "capt_am@gmail.com", "09171234567");

console.log(output);